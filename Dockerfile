 # Stage 1
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim As base
WORKDIR /app
EXPOSE 80
EXPOSE 443
 
 # Stage 2
 FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS builder
 WORKDIR /source

 # caches restore result by copying csproj file separately
 COPY *.csproj .
 RUN dotnet restore

 # copies the rest of your code
 COPY . .
 RUN dotnet build "WeatherApi.csproj" -c Release -o /app/build

 # stage 3
 FROM builder As publish
 RUN dotnet publish "WeatherApi.csproj" -c Release -o /app/publish

 # Stage 4
 FROM base As final
 WORKDIR /app
 COPY --from=publish /app/publish .
 ENTRYPOINT ["dotnet", "WeatherApi.dll"]



#FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim As base
#WORKDIR /app
#EXPOSE 80
#EXPOSE 443
#
#FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster As build
#WORKDIR /src
#COPY . .
#RUN dotnet build "WeatherApi.csproj" -c Relase -o /app/build
#
#FROM build As publish
#RUN dotnet publish "WeatherApi.csproj" -c Release -o /app/publish
#
#FROM base As final
#WORKDIR /app
#COPY --from=publish /app/publish .
#ENTRYPOINT ["dotnet","WeatherApi.dll"]


